package ramin.masoumi.test.usecase

import kotlinx.coroutines.flow.Flow
import ramin.masoumi.data.CallResult
import ramin.masoumi.data.model.FeedItem
import ramin.masoumi.test.repository.FeedRepository
import javax.inject.Inject

/**
 * UseCase for feed page
 */
interface FeedUseCase {
    /**
     * get the feed from its link
     * @param link of the feed
     */
    fun getFeed(link: String): Flow<CallResult<FeedItem>>
}
class FeedUseCaseImpl @Inject constructor(private val repository: FeedRepository): FeedUseCase{
    override fun getFeed(link: String): Flow<CallResult<FeedItem>> {
        return repository.get(link)
    }

}