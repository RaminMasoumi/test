package ramin.masoumi.test.usecase

import kotlinx.coroutines.flow.Flow
import ramin.masoumi.data.CallResult
import ramin.masoumi.data.model.FeedItem
import ramin.masoumi.test.repository.FeedRepository
import javax.inject.Inject

/**
 * UseCase for FeedList page
 */
interface FeedListUseCase {
    /**
     * get list of Feeds that include one or more of the tags in list
     * @param tags list of tags
     */
    fun getFeedList(tags: List<String>): Flow<CallResult<List<FeedItem>>>
}
class FeedListUseCaseImpl @Inject constructor(private val repository: FeedRepository)
    : FeedListUseCase{
    override fun getFeedList(tags: List<String>): Flow<CallResult<List<FeedItem>>> {
        return repository.getList(tags)
    }
}