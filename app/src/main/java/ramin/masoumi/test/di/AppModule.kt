package ramin.masoumi.test.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import ramin.masoumi.data.DataConfig
import ramin.masoumi.data.DataProvider
import ramin.masoumi.test.R
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Singleton
    @Provides
    fun provideDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Singleton
    @Provides
    fun provideDataConfig(@ApplicationContext context: Context): DataConfig {
        return DataConfig(baseUrl = context.getString(R.string.base_url))
    }

    @Singleton
    @Provides
    fun provideDataProvider(@ApplicationContext context: Context,
                            config: DataConfig): DataProvider {
        return DataProvider(context = context, config = config)
    }
}