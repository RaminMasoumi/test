package ramin.masoumi.test.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ramin.masoumi.test.repository.FeedRepository
import ramin.masoumi.test.repository.FeedRepositoryImpl

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {
    @Binds
    fun bindFeedRepository(impl: FeedRepositoryImpl): FeedRepository
}