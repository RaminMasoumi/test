package ramin.masoumi.test.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ramin.masoumi.test.usecase.FeedListUseCase
import ramin.masoumi.test.usecase.FeedListUseCaseImpl
import ramin.masoumi.test.usecase.FeedUseCase
import ramin.masoumi.test.usecase.FeedUseCaseImpl

@Module
@InstallIn(SingletonComponent::class)
interface UseCaseModule {
    @Binds
    fun bindFeedUseCase(impl: FeedUseCaseImpl): FeedUseCase

    @Binds
    fun bindFeedListUseCase(impl: FeedListUseCaseImpl): FeedListUseCase
}