package ramin.masoumi.test.repository

import kotlinx.coroutines.flow.Flow
import ramin.masoumi.data.CallResult
import ramin.masoumi.data.DataProvider
import ramin.masoumi.data.model.FeedItem
import javax.inject.Inject

/**
 * Repository to provide Feed data
 */
interface FeedRepository {
    /**
     * get a list of Feeds that have one or more of the provided tags
     * @param tags list of feed tags
     * @return a flow of list of feeds
     */
    fun getList(tags: List<String>): Flow<CallResult<List<FeedItem>>>

    /**
     * get a list of Feeds that have one or more of the provided tags
     * @param link link of the feed
     * @return a flow of a feeds
     */
    fun get(link: String): Flow<CallResult<FeedItem>>
}

class FeedRepositoryImpl @Inject constructor(private val dataProvider: DataProvider)
    : FeedRepository{
    override fun getList(tags: List<String>): Flow<CallResult<List<FeedItem>>> {
        return dataProvider.getFeed(tags)
    }

    override fun get(link: String): Flow<CallResult<FeedItem>> {
        return dataProvider.getFeedItem(link)
    }

}