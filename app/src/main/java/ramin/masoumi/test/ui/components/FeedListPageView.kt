package ramin.masoumi.test.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.valentinilk.shimmer.shimmer
import ramin.masoumi.data.CallResult
import ramin.masoumi.data.model.FeedItem
import ramin.masoumi.test.R
import ramin.masoumi.test.util.iconId
import ramin.masoumi.test.util.urlId
import java.time.Instant
import java.time.ZonedDateTime

/**
 * entry point for FeedList view UI page
 * @param callResult result of a call for data
 * @param tags space separated list of tags
 * @param search handle to search for new feeds
 * @param navigateToFeed handle to open a feed page
 */
@Composable
fun FeedListPageView(callResult: CallResult<List<FeedItem>>,
                     tags: String,
                     setTags: (String) -> Unit,
                     search: () -> Unit,
                     navigateToFeed: (String) -> Unit) {
    val focusManager = LocalFocusManager.current
    Column(modifier = Modifier.padding(8.dp, 0.dp)) {
        Spacer(modifier = Modifier.height(5.dp))
        TitleText(title = R.string.lbl_feed_list, modifier = Modifier.fillMaxWidth())
        Spacer(modifier = Modifier.height(5.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {
            OutlinedTextField(modifier = Modifier.weight(1f), value = tags, onValueChange = setTags,
                label = {
                    Text(text = stringResource(id = R.string.lbl_search_tags))
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {
                        search()
                        focusManager.clearFocus()
                    }
                ),)
            Spacer(modifier = Modifier.width(5.dp))
            IconButton(onClick = {
                search()
                focusManager.clearFocus()
            }) {
                Icon(imageVector = Icons.Filled.Search,
                    contentDescription = stringResource(id = R.string.desc_img_search),
                    modifier = Modifier.semantics { iconId = "search" })
            }
        }
        Spacer(modifier = Modifier.height(5.dp))
        when (callResult.status) {
            CallResult.Status.SUCCESS -> {
                val list = callResult.data
                if (list.isNullOrEmpty()) {
                    Text(
                        text = stringResource(R.string.info_no_feed_found),
                        modifier = Modifier.fillMaxWidth()
                    )
                } else {
                    FeedListView(list = list, navigateToFeed = navigateToFeed)
                }
            }
            CallResult.Status.LOADING, CallResult.Status.IDLE -> {
                LoadingList()
            }
            CallResult.Status.ERROR -> {
                Text(
                    text = stringResource(R.string.error_network_failure),
                    modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.bodyMedium.copy(color = Color(R.color.error))
                )
            }
        }
    }
}

/**
 * Component that composes feed data
 * @param list data of list of feeds
 * @param navigateToFeed handle for forward navigation
 */
@Composable
private fun FeedListView(list: List<FeedItem>, navigateToFeed: (String) -> Unit) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        modifier = Modifier.fillMaxWidth()
    ) {
        items(items = list) { feed ->
            FeedView(feed = feed) {
                navigateToFeed(feed.link)
            }
        }
    }
}

/**
 * Component that composes one feed item
 * @param feed data of the actual feed
 * @param navigateToFeed navigate to feed
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun FeedView(feed: FeedItem, navigateToFeed: () -> Unit) {
    Card(
        onClick = navigateToFeed,
        modifier = Modifier.padding(5.dp),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
        )
    ) {
        Column {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(feed.media)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(R.drawable.ic_launcher_foreground),
                contentDescription = stringResource(R.string.desc_img_feed),
                modifier = Modifier
                    .semantics { urlId = feed.media }
                    .clip(RoundedCornerShape(5.dp))
                    .fillMaxWidth()
                    .aspectRatio(1f)
            )
            Text(
                text = feed.title,
                modifier = Modifier
                    .padding(2.dp, 5.dp)
                    .fillMaxWidth(),
                textAlign= TextAlign.Center,
                style = MaterialTheme.typography.labelMedium
            )
        }
    }

}

/**
 * Component that shows a skeleton loading of a list
 */
@Composable
private fun LoadingList() {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        modifier = Modifier.fillMaxWidth()
    ) {
        items(5) { _ ->
            LoadingItem()
        }
    }
}

/**
 * Component that composes loading view of one item
 */
@Composable
private fun LoadingItem() {
    Card(
        modifier = Modifier.padding(5.dp),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
        )
    ) {
        Column {
            Box(
                modifier = Modifier
                    .clip(RoundedCornerShape(5.dp))
                    .fillMaxWidth()
                    .aspectRatio(1f)
                    .background(MaterialTheme.colorScheme.surface)
                    .shimmer()
            )
            Box(
                modifier = Modifier
                    .padding(2.dp, 5.dp)
                    .height(12.dp)
                    .fillMaxWidth(0.6f)
                    .background(MaterialTheme.colorScheme.surface)
                    .shimmer()
            )
        }
    }
}

/**
 * Preview of the entire page
 */
@Preview(showBackground = true)
@Composable
private fun PagePreview() {
    val feed = FeedItem(
        title = "DSC_7097-Enhanced-NR.jpg",
        link = "https://www.flickr.com/photos/asampson/53126131172/",
        media = "https://live.staticflickr.com/65535/53126131172_a8d993e248_m.jpg",
        dateTaken = ZonedDateTime.parse("2023-08-19T19:51:44-08:00"),
        description = "<p><a href=\"https://www.flickr.com/people/asampson/\">" +
                "Andrew Sampson (andrewtakeslotsofphotos) on insta</a> posted a photo:</p> " +
                "<p><a href=\"https://www.flickr.com/photos/asampson/53126131172/\" " +
                "title=\"DSC_7097-Enhanced-NR.jpg\"><img " +
                "src=\"https://live.staticflickr.com/65535/53126131172_a8d993e248_m.jpg\" " +
                "width=\"192\" height=\"240\" alt=\"DSC_7097-Enhanced-NR.jpg\" /></a></p>",
        published = Instant.parse("2023-08-19T10:21:16Z"),
        author = "nobody@flickr.com (\"Andrew Sampson (andrewtakeslotsofphotos) on insta\")",
        authorId = "29831878@N06",
        tags = listOf("feed","tag")
    )
    val list = CallResult.success(listOf(feed, feed, feed))
    FeedListPageView(callResult = list, tags = "tag tag", {}, {}) {}
}

/**
 * Preview of loading component
 */
@Preview(showBackground = true)
@Composable
private fun LoadPreview() {
    LoadingList()
}