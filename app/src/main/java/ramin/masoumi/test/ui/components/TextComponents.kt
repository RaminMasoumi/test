package ramin.masoumi.test.ui.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

/**
 * A component of a label text with a dynamic text below
 * @param lbl resource id of the label
 * @param text string for body of the labeled message
 */
@Composable
fun LabeledText(@StringRes lbl: Int, text: String) {
    Card(
        modifier = Modifier
            .padding(2.dp)
            .fillMaxWidth(),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surface
        )
    ) {
        Column(
            modifier = Modifier
                .padding(5.dp)
        ) {
            Text(
                text = stringResource(lbl),
                modifier = Modifier.padding(2.dp),
                style = MaterialTheme.typography.bodyLarge
            )
            Text(
                text = text,
                modifier = Modifier.padding(2.dp),
                style = MaterialTheme.typography.bodySmall
            )
        }

    }
}

/**
 * A component of a title text
 * @param title string resource id of the title
 * @param modifier component modifier
 */
@Composable
fun TitleText(@StringRes title: Int, modifier: Modifier = Modifier) {
    TitleText(title = stringResource(title), modifier = modifier)
}

/**
 * A component of a title text
 * @param title string of the title
 * @param modifier component modifier
 */
@Composable
fun TitleText(title: String, modifier: Modifier = Modifier) {
    Text(
        text = title,
        textAlign = TextAlign.Center,
        modifier = modifier,
        style = MaterialTheme.typography.displaySmall
    )
}