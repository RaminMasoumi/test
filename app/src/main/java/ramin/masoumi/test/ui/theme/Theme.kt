package ramin.masoumi.test.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.colorResource
import ramin.masoumi.test.R

@Composable
fun darkColorPalette() = darkColorScheme(
    primary = colorResource(R.color.primary),
    inversePrimary = colorResource(R.color.primary2),
    secondary = colorResource(R.color.secondary),
    background = colorResource(R.color.background),
    surface = colorResource(R.color.surface)
)

@Composable
fun lightColorPalette() = lightColorScheme(
    primary = colorResource(R.color.primary),
    inversePrimary = colorResource(R.color.primary2),
    secondary = colorResource(R.color.secondary),
    background = colorResource(R.color.background),
    surface = colorResource(R.color.surface)
)

@Composable
fun TestTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        darkColorPalette()
    } else {
        lightColorPalette()
    }

    MaterialTheme(
        colorScheme = colors,
        typography = fontTypography(),
        shapes = Shapes,
        content = content
    )
}