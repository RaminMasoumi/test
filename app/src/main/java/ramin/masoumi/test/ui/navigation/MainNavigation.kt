package ramin.masoumi.test.ui.navigation

import androidx.annotation.VisibleForTesting
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import ramin.masoumi.test.ui.components.FeedPageView
import ramin.masoumi.test.ui.components.FeedListPageView
import ramin.masoumi.test.ui.navigation.MainNavigation.Feed.navigateToFeed
import ramin.masoumi.test.viewmodel.FeedListViewModel
import ramin.masoumi.test.viewmodel.FeedViewModel
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

/**
 * MainNavigation graph that contains first app page
 */
sealed class MainNavigation(override val route: String) : Navigation(route) {
    override val graphId = "Root"

    companion object {
        /**
         * get all destinations on this graph
         */
        fun getAllNavigation() = setOf(
            FeedList,
            Feed
        )
    }

    /**
     * List of Feed page
     */
    object FeedList : MainNavigation("FEED_LIST") {
        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        object ArgKeys {
            const val TAGS = "TAGS"
        }

        class Arguments(savedStateHandle: SavedStateHandle) {
            val tags = SavedArgument(savedStateHandle, ArgKeys.TAGS, "")
        }

        fun NavController.navigateToFeedList() {
            navigate(route) {
                popUpTo(graph.id) {
                    inclusive = true
                }
            }
        }

        context(NavGraphBuilder)
        override fun compose(controller: NavController) {
            composable(route = getFullRoute(), arguments = getArguments()) {
                val viewModel: FeedListViewModel = hiltViewModel()
                val feedList by viewModel.feedList.collectAsStateWithLifecycle()
                val tagQuery by viewModel.tagQuery.collectAsStateWithLifecycle()
                FeedListPageView(
                    callResult = feedList,
                    tags = tagQuery,
                    setTags = viewModel::setTags,
                    search = viewModel::search) {
                    controller.navigateToFeed(it)
                }
            }
        }
    }

    object Feed : MainNavigation("FEED") {
        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        object ArgKeys {
            const val FEED_LINK = "FEED_LINK"
        }

        class Arguments(savedStateHandle: SavedStateHandle) {
            val link = SavedArgument(savedStateHandle, ArgKeys.FEED_LINK, "invalid")
        }

        fun NavController.navigateToFeed(url: String) {
            val encoded = URLEncoder.encode(url, StandardCharsets.UTF_8.toString())
            navigate(route = "$route/$encoded")
        }

        override fun getArguments() = listOf(
            navArgument(ArgKeys.FEED_LINK) {
                type = NavType.StringType
                defaultValue = ""
            }
        )

        context(NavGraphBuilder)
        override fun compose(controller: NavController) {
            composable(route = getFullRoute(), arguments = getArguments()) {
                val viewModel: FeedViewModel = hiltViewModel()
                val feed by viewModel.feed.collectAsStateWithLifecycle()
                FeedPageView(callResult = feed)
            }
        }
    }
}