package ramin.masoumi.test.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.integerResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import ramin.masoumi.test.R

@Composable
fun fontTypography() = Typography(
    displayLarge = fontDisplayLarge(),
    displayMedium = fontDisplayMedium(),
    displaySmall = fontDisplaySmall(),
    headlineLarge = fontHeadLarge(),
    headlineMedium = fontHeadMedium(),
    headlineSmall = fontHeadSmall(),
    titleLarge = fontTitleLarge(),
    titleMedium = fontTitleMedium(),
    titleSmall = fontTitleSmall(),
    bodyLarge = fontBodyLarge(),
    bodyMedium = fontBodyMedium(),
    bodySmall = fontBodySmall(),
    labelLarge = fontLabelLarge(),
    labelMedium = fontLabelMedium(),
    labelSmall = fontLabelSmall()
)
@Composable
private fun fontDefault() = TextStyle(
    fontSize = dimensionResource(id = R.dimen.font_size_default).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_default)),
    color = colorResource(id = R.color.text),
    fontFamily = FontFamily.SansSerif,
)

@Composable
private fun fontDisplayLarge() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_display_large).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_super_bold)),
)

@Composable
private fun fontDisplayMedium() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_display_medium).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_bold)),
)

@Composable
private fun fontDisplaySmall() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_display_small).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_default)),
)

@Composable
private fun fontHeadLarge() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_head_large).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_super_bold)),
)

@Composable
private fun fontHeadMedium() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_head_medium).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_bold)),
)

@Composable
private fun fontHeadSmall() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_head_small).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_default)),
)

@Composable
private fun fontTitleLarge() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_title_large).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_bold)),
)

@Composable
private fun fontTitleMedium() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_title_medium).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_default)),
)

@Composable
private fun fontTitleSmall() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_title_small).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_thin)),
)

@Composable
private fun fontBodyLarge() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_body_large).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_bold)),
)

@Composable
private fun fontBodyMedium() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_body_medium).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_default)),
)

@Composable
private fun fontBodySmall() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_body_small).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_thin)),
)

@Composable
private fun fontLabelLarge() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_lbl_large).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_default)),
)

@Composable
private fun fontLabelMedium() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_lbl_medium).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_thin)),
)

@Composable
private fun fontLabelSmall() = fontDefault().copy(
    fontSize = dimensionResource(id = R.dimen.font_size_lbl_small).value.sp,
    fontWeight = FontWeight(integerResource(id = R.integer.font_weight_super_thin)),
)