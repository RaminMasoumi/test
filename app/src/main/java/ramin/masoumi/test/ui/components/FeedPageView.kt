package ramin.masoumi.test.ui.components

import android.widget.TextView
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.text.HtmlCompat
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.valentinilk.shimmer.shimmer
import ramin.masoumi.data.CallResult
import ramin.masoumi.data.model.FeedItem
import ramin.masoumi.test.R
import ramin.masoumi.test.util.Util
import ramin.masoumi.test.util.urlId
import java.time.Instant
import java.time.ZonedDateTime

/**
 * entry point for Feed view UI page
 * @param callResult result of a call for data
 */
@Composable
fun FeedPageView(callResult: CallResult<FeedItem>) {
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(20.dp, 0.dp)
        .verticalScroll(rememberScrollState())) {
        Spacer(modifier = Modifier.height(5.dp))
        when (callResult.status) {
            CallResult.Status.SUCCESS -> {
                callResult.data.let {
                    requireNotNull(it)
                    FeedView(it)
                }
            }
            CallResult.Status.LOADING, CallResult.Status.IDLE -> {
                LoadingFeedView()
            }
            CallResult.Status.ERROR -> {
                Text(
                    text = stringResource(id = R.string.err_not_found),
                    style = MaterialTheme.typography.labelMedium.copy(color = Color(R.color.error))
                )
            }
        }
    }
}

/**
 * Component that composes feed data
 * @param feed data of the actual feed
 */
@Composable
private fun FeedView(feed : FeedItem) {
    Column(modifier = Modifier.fillMaxSize()) {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data(feed.media)
                .crossfade(true)
                .build(),
            placeholder = painterResource(R.drawable.ic_launcher_foreground),
            contentDescription = stringResource(R.string.desc_img_feed),
            modifier = Modifier
                .semantics { urlId = feed.media }
                .clip(RoundedCornerShape(5.dp))
                .fillMaxWidth()
                .aspectRatio(1f)
                .background(color = MaterialTheme.colorScheme.inversePrimary)
        )
        Spacer(modifier = Modifier.height(5.dp))
        if(feed.title.isNotBlank()) {
            LabeledText(lbl = R.string.lbl_title, text = feed.title)
        }
        LabeledText(lbl = R.string.lbl_link, text = feed.link)
        LabeledText(lbl = R.string.lbl_date_taken, text = Util.dateFormatter.format(feed.dateTaken))
        LabeledText(lbl = R.string.lbl_published, text = Util.dateFormatter.format(feed.published))
        LabeledText(lbl = R.string.lbl_author, text = feed.author)
        if(feed.description.isNotBlank()) {
            val html = HtmlCompat.fromHtml(feed.description, 0)
            AndroidView(
                factory = { TextView(it) },
                update = { it.text = html }
            )
        }
        if(feed.tags.isNotEmpty()) {
            TagListView(list = feed.tags)
        }
        Spacer(modifier = Modifier.height(10.dp))
    }
}

@OptIn(ExperimentalLayoutApi::class)
@Composable
private fun TagListView(list: List<String>) {
    FlowRow(
        modifier = Modifier.fillMaxWidth()
    ) {
        list.forEach {
            Text(text = it, modifier = Modifier
                .padding(10.dp)
                .background(color = MaterialTheme.colorScheme.inversePrimary,
                    shape = MaterialTheme.shapes.large)
                .padding(10.dp),
                style = MaterialTheme.typography.bodySmall)
        }
    }
}


/**
 * Component that shows a skeleton loading instead of feed
 */
@Composable
private fun LoadingFeedView() {
    Column(modifier = Modifier.fillMaxSize()) {
        Box(
            modifier = Modifier
                .clip(RoundedCornerShape(5.dp))
                .fillMaxWidth()
                .aspectRatio(1f)
                .background(color = MaterialTheme.colorScheme.inversePrimary)
                .shimmer()
        )
        Spacer(modifier = Modifier.height(5.dp))
        Box(modifier = Modifier
            .padding(0.dp, 5.dp)
            .height(20.dp)
            .background(color = MaterialTheme.colorScheme.surface)
            .fillMaxWidth(0.4f)
            .shimmer())
        Box(modifier = Modifier
            .padding(0.dp, 5.dp)
            .height(20.dp)
            .background(color = MaterialTheme.colorScheme.surface)
            .fillMaxWidth(0.8f)
            .shimmer())
        Box(modifier = Modifier
            .padding(0.dp, 5.dp)
            .height(20.dp)
            .background(color = MaterialTheme.colorScheme.surface)
            .fillMaxWidth(0.5f)
            .shimmer())
        Box(modifier = Modifier
            .padding(0.dp, 5.dp)
            .height(20.dp)
            .background(color = MaterialTheme.colorScheme.surface)
            .fillMaxWidth(0.3f)
            .shimmer())
        Box(modifier = Modifier
            .padding(0.dp, 5.dp)
            .height(20.dp)
            .background(color = MaterialTheme.colorScheme.surface)
            .fillMaxWidth(0.3f)
            .shimmer())
        Box(modifier = Modifier
            .padding(0.dp, 5.dp)
            .height(20.dp)
            .background(color = MaterialTheme.colorScheme.surface)
            .fillMaxWidth(0.7f)
            .shimmer())
    }
}

/**
 * Preview of the entire page
 */
@Preview(showBackground = true)
@Composable
private fun PagePreview() {
    val feed = FeedItem(
        title = "DSC_7097-Enhanced-NR.jpg",
        link = "https://www.flickr.com/photos/asampson/53126131172/",
        media = "https://live.staticflickr.com/65535/53126131172_a8d993e248_m.jpg",
        dateTaken = ZonedDateTime.parse("2023-08-19T19:51:44-08:00"),
        description = "<p><a href=\"https://www.flickr.com/people/asampson/\">" +
                "Andrew Sampson (andrewtakeslotsofphotos) on insta</a> posted a photo:</p> " +
                "<p><a href=\"https://www.flickr.com/photos/asampson/53126131172/\" " +
                "title=\"DSC_7097-Enhanced-NR.jpg\"><img " +
                "src=\"https://live.staticflickr.com/65535/53126131172_a8d993e248_m.jpg\" " +
                "width=\"192\" height=\"240\" alt=\"DSC_7097-Enhanced-NR.jpg\" /></a></p>",
        published = Instant.parse("2023-08-19T10:21:16Z"),
        author = "nobody@flickr.com (\"Andrew Sampson (andrewtakeslotsofphotos) on insta\")",
        authorId = "29831878@N06",
        tags = listOf("feed","tag")
    )
    FeedPageView(CallResult.success(feed))
}

/**
 * Preview of loading component
 */
@Preview(showBackground = true)
@Composable
private fun LoadPreview() {
    LoadingFeedView()
}