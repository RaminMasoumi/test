package ramin.masoumi.test.util

import java.time.format.DateTimeFormatter
import java.util.TimeZone

object Util {
    /**
     * DateFormatter to format date objects to string
     */
    val dateFormatter: DateTimeFormatter =
        DateTimeFormatter.RFC_1123_DATE_TIME.withZone(TimeZone.getDefault().toZoneId())
}