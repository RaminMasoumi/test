package ramin.masoumi.test.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.plus
import ramin.masoumi.data.CallResult
import ramin.masoumi.test.ui.navigation.MainNavigation
import ramin.masoumi.test.usecase.FeedListUseCase
import javax.inject.Inject

/**
 * ViewModel for FeedList page
 * @param savedStateHandle for saved arguments
 * @param dispatcherIo IO dispatcher for network call
 * @param useCase FeedList useCase
 */
@HiltViewModel
class FeedListViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    dispatcherIo: CoroutineDispatcher,
    private val useCase: FeedListUseCase,
) : ViewModel() {
    //Page arguments
    private val args = MainNavigation.FeedList.Arguments(savedStateHandle)
    //query of space separated tags
    val tagQuery = args.tags.flow
    //list of tags used to look up Feeds
    private val tags = MutableStateFlow(emptyList<String>())
    //flow of the list of feeds that have one or more of the tags
    @OptIn(ExperimentalCoroutinesApi::class)
    val feedList = tags.flatMapLatest {
        useCase.getFeedList(it)
    }.stateIn(viewModelScope + dispatcherIo, SharingStarted.Eagerly, CallResult.loading())

    //trigger feed search using tagQuery
    fun search() {
        val tag = tagQuery.value
        tags.value = tag.split("\\s".toRegex())
    }


    //set tagQuery
    fun setTags(tags: String) {
        args.tags.set(tags)
    }
}