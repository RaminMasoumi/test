package ramin.masoumi.test.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.plus
import ramin.masoumi.data.CallResult
import ramin.masoumi.test.ui.navigation.MainNavigation
import ramin.masoumi.test.usecase.FeedUseCase
import javax.inject.Inject

/**
 * ViewModel for FeedPage
 * @param savedStateHandle for saved arguments
 * @param dispatcherIo IO dispatcher for network call
 * @param useCase Feed useCase
 */
@HiltViewModel
class FeedViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    dispatcherIo: CoroutineDispatcher,
    useCase: FeedUseCase,
) : ViewModel() {
    //saved arguments
    private val args = MainNavigation.Feed.Arguments(savedStateHandle)
    //link of the Feed
    private val link = args.link.get()
    //Flow of the feed data
    val feed = useCase.getFeed(link)
        .stateIn(viewModelScope + dispatcherIo, SharingStarted.Eagerly, CallResult.loading())
}