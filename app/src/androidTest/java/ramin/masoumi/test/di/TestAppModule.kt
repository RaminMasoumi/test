package ramin.masoumi.test.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import ramin.masoumi.data.DataConfig
import ramin.masoumi.data.DataProvider
import ramin.masoumi.test.util.TestUtil.readFile
import javax.inject.Singleton

@OptIn(ExperimentalCoroutinesApi::class)
@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [AppModule::class]
)
class TestAppModule {
    @Singleton
    @Provides
    fun provideDispatcher(): CoroutineDispatcher = UnconfinedTestDispatcher()

    @Singleton
    @Provides
    fun provideDataConfig(): DataConfig {
        val mockServer = MockWebServer()
        val c = this
        mockServer.dispatcher = object : Dispatcher(){
            override fun dispatch(request: RecordedRequest): MockResponse = when (request.path) {
                "/services/feeds/photos_public.gne?format=" +
                        "json&nojsoncallback=1&safe_search=1&tags="
                -> MockResponse().setResponseCode(200)
                    .setBody("feed.json".readFile(c))
                else -> MockResponse().setResponseCode(404)
            }
        }
        return DataConfig(baseUrl = mockServer.url("/").toString())
    }

    @Singleton
    @Provides
    fun provideDataProvider(@ApplicationContext context: Context,
                            config: DataConfig
    ): DataProvider {
        return DataProvider(context = context, config = config)
    }
}