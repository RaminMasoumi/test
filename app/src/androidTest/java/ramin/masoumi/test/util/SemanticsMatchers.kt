package ramin.masoumi.test.util

import androidx.compose.ui.test.SemanticsMatcher

fun hasUrlString(url : String): SemanticsMatcher =
    SemanticsMatcher.expectValue(UrlString, url)

fun hasIconId(id : String): SemanticsMatcher =
    SemanticsMatcher.expectValue(IconId, id)