package ramin.masoumi.test

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.espresso.Espresso
import androidx.test.ext.junit.runners.AndroidJUnit4
import ramin.masoumi.test.util.TestUtil.waitUntilExists
import ramin.masoumi.test.util.hasIconId
import ramin.masoumi.test.util.hasUrlString
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class MainActivityTest {
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val activityRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun testFeedList(){
        activityRule.waitUntilExists(
            hasUrlString("https://live.staticflickr.com/65535/53128736182_81bef2a53d_m.jpg"))
        activityRule.onNodeWithText("Enzian").assertExists()
        activityRule.onNode(hasUrlString(
            "https://live.staticflickr.com/65535/53128736182_81bef2a53d_m.jpg")).assertExists()
        activityRule.onNode(hasUrlString(
            "https://live.staticflickr.com/65535/53128736882_e399fc16dc_m.jpg")).assertExists()
        activityRule.onNode(hasUrlString(
            "https://live.staticflickr.com/65535/53128737372_3467dd7d5a_m.jpg")).assertExists()
        activityRule.onNode(hasUrlString(
            "https://live.staticflickr.com/65535/53128737567_c3ca7c3914_m.jpg")).assertExists()
        activityRule.onNode(hasUrlString(
            "https://live.staticflickr.com/65535/53128738042_b0c25b2deb_m.jpg")).assertExists()
    }

    @Test
    fun testFeed(){
        activityRule.waitUntilExists(hasUrlString(
            "https://live.staticflickr.com/65535/53128736182_81bef2a53d_m.jpg"))
        activityRule.onNodeWithText("Enzian").performClick()
        activityRule.waitUntilExists(hasUrlString(
            "https://live.staticflickr.com/65535/53128736182_81bef2a53d_m.jpg"))
        activityRule.onNodeWithText("Enzian").assertExists()
        activityRule.onNodeWithText("nobody@flickr.com (\"naturgucker.de\")").assertExists()
        activityRule.onNodeWithText("ngid1693414413").assertExists()
        activityRule.onNodeWithText("gentianaindet").assertExists()
        activityRule.onNodeWithText("enzianunbestimmt").assertExists()
    }

    @Test
    fun testNavigation() {
        activityRule.onNode(hasIconId("search")).assertExists()
        activityRule.waitUntilExists(hasUrlString(
            "https://live.staticflickr.com/65535/53128736182_81bef2a53d_m.jpg"))
        activityRule.onNodeWithText("Enzian").performClick()
        Espresso.pressBack()
        activityRule.onNode(hasIconId("search")).assertExists()
    }

}