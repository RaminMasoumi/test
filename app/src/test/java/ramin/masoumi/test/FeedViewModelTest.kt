package ramin.masoumi.test

import androidx.lifecycle.SavedStateHandle
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.junit.Assert
import org.junit.Test
import ramin.masoumi.data.CallResult
import ramin.masoumi.data.model.FeedItem
import ramin.masoumi.test.ui.navigation.MainNavigation
import ramin.masoumi.test.usecase.FeedUseCase
import ramin.masoumi.test.viewmodel.FeedViewModel
import java.time.Instant
import java.time.ZonedDateTime

@OptIn(ExperimentalCoroutinesApi::class)
class FeedViewModelTest {
    private val savedStateHandle = mockk<SavedStateHandle>()
    private val useCase = mockk<FeedUseCase>()

    @Test
    fun `feed is fetched correctly`() {
        val feedItem = FeedItem(
            title = "DSC_7097-Enhanced-NR.jpg",
            link = "https://www.flickr.com/photos/asampson/53126131172/",
            media = "https://live.staticflickr.com/65535/53126131172_a8d993e248_m.jpg",
            dateTaken = ZonedDateTime.parse("2023-08-19T19:51:44-08:00"),
            description = "<p><a href=\"https://www.flickr.com/people/asampson/\">" +
                    "Andrew Sampson (andrewtakeslotsofphotos) on insta</a> posted a photo:</p> " +
                    "<p><a href=\"https://www.flickr.com/photos/asampson/53126131172/\" " +
                    "title=\"DSC_7097-Enhanced-NR.jpg\"><img " +
                    "src=\"https://live.staticflickr.com/65535/53126131172_a8d993e248_m.jpg\" " +
                    "width=\"192\" height=\"240\" alt=\"DSC_7097-Enhanced-NR.jpg\" /></a></p>",
            published = Instant.parse("2023-08-19T10:21:16Z"),
            author = "nobody@flickr.com (\"Andrew Sampson (andrewtakeslotsofphotos) on insta\")",
            authorId = "29831878@N06",
            tags = listOf("feed","tag")
        )
        savedStateHandle.mockReturn(MainNavigation.Feed.ArgKeys.FEED_LINK, "link")
        every { useCase.getFeed("link") }.returns( flow {
            emit(CallResult.success(feedItem))
        })
        val feedViewModel = FeedViewModel(savedStateHandle, UnconfinedTestDispatcher(), useCase)
        Assert.assertEquals(feedItem, feedViewModel.feed.value.data)
    }
}