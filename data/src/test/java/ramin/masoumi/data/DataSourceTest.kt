package ramin.masoumi.data

import androidx.test.core.app.ApplicationProvider
import com.squareup.moshi.Moshi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import ramin.masoumi.data.TestUtil.readFile
import ramin.masoumi.data.di.DaggerTestAppComponent
import ramin.masoumi.data.model.Feed
import ramin.masoumi.data.model.FeedItem
import ramin.masoumi.data.persist.CacheDatabase
import ramin.masoumi.data.source.DataSource
import java.io.IOException
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(RobolectricTestRunner::class)
class DataSourceTest {
    @Inject
    internal lateinit var dataSource: DataSource
    @Inject
    internal lateinit var db: CacheDatabase
    @Inject
    internal lateinit var moshi: Moshi

    @Before
    fun init() {
        val mockServer = MockWebServer()
        val c = this
        mockServer.dispatcher = object : Dispatcher(){
            override fun dispatch(request: RecordedRequest): MockResponse = when (request.path) {
                "/services/feeds/photos_public.gne?format=" +
                        "json&nojsoncallback=1&safe_search=1&tags=tag"
                -> MockResponse().setResponseCode(200)
                    .setBody("feed.json".readFile(c))
                else -> MockResponse().setResponseCode(404)
            }
        }
        DaggerTestAppComponent.builder().context(ApplicationProvider.getApplicationContext())
            .config(DataConfig(baseUrl = mockServer.url("/").toString())).build().inject(this)
    }

    @After
    @Throws(IOException::class)
    fun destroy() {
        db.close()
    }

    @Test
    fun testFeed() = runTest {
        testRequest("feed.json", Feed::class.java, { it.items }) {
            dataSource.getFeed(listOf("tag"))
        }
    }

    @Test
    fun testFeedItem() = runTest {
        saveFileToDb("feeditem.json", FeedItem::class.java) {
            db.feedDao().insertFeedItemWithTag(it)
        }
        testRequest("feeditem.json", FeedItem::class.java, { it }) {
            dataSource.getFeedItem("https://www.flickr.com/photos/12639178@N07/53128736182/")
        }
    }

    private suspend fun <T> saveFileToDb(fileName: String,
                                     adapterType: Class<T>,
                                     save: suspend (T) -> Unit) {
        val adapter = moshi.adapter(adapterType)
        val json = adapter.fromJson(fileName.readFile(this@DataSourceTest))!!
        save(json)
    }

    private suspend fun <T, A> testRequest(
        fileName: String, adapterType: Class<T>,
        converter: (T) -> A, request: () -> Flow<CallResult<A>>
    ) {
        val adapter = moshi.adapter(adapterType)
        val json = adapter.fromJson(fileName.readFile(this@DataSourceTest))!!
        val flow = request()
        val results = flow.filter { it.isSuccess() }.first()
        Assert.assertEquals(converter(json), results.data)
    }
}