package ramin.masoumi.data

object TestUtil {
    fun String.readFile(c: Any) : String =
        c.javaClass.classLoader!!.getResource(this).readText()
}