package ramin.masoumi.data

import com.squareup.moshi.Moshi
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import ramin.masoumi.data.TestUtil.readFile
import ramin.masoumi.data.di.DaggerTestAppComponent
import ramin.masoumi.data.model.Feed
import ramin.masoumi.data.net.Api
import retrofit2.Response
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
class ApiTest {
    @Inject internal lateinit var api: Api
    @Inject internal lateinit var moshi: Moshi
    @Before
    fun init() {
        val mockServer = MockWebServer()
        val c = this
        mockServer.dispatcher = object : Dispatcher(){
            override fun dispatch(request: RecordedRequest): MockResponse = when (request.path) {
                "/services/feeds/photos_public.gne?format=" +
                        "json&nojsoncallback=1&safe_search=1&tags=tag"
                -> MockResponse().setResponseCode(200)
                    .setBody("feed.json".readFile(c))
                else -> MockResponse().setResponseCode(404)
            }
        }
        DaggerTestAppComponent.builder().context(mockk(relaxed = true))
            .config(DataConfig(baseUrl = mockServer.url("/").toString())).build().inject(this)
    }

    @Test
    fun testFeeds()= runTest {
        testRequest("feed.json", Feed::class.java) {api.getFeed("tag")}
    }

    private suspend fun <T : Any> testRequest(fileName : String, adapterType : Class<T>, request :
    suspend()-> Response<T>){
        val adapter = moshi.adapter(adapterType)
        val input = javaClass.classLoader!!.getResource(fileName).readText()
        val json = adapter.fromJson(input)!!
        val response = request().body()
        Assert.assertEquals(json, response)
    }
}