package ramin.masoumi.data.di

import android.content.Context
import androidx.room.Room
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import ramin.masoumi.data.persist.CacheDatabase
import ramin.masoumi.data.persist.Converter
import ramin.masoumi.data.persist.FeedItemDao
import javax.inject.Singleton

@Module
internal class TestDatabaseModule {
    /**
     * provides room database instance
     */
    @Singleton
    @Provides
    fun providesRoomDatabase(context: Context, moshi: Moshi): CacheDatabase {
        return Room.inMemoryDatabaseBuilder(
            context,
            CacheDatabase::class.java
        ).addTypeConverter(Converter(moshi))
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration().build()
    }

    /**
     * provides room FeedDao
     */
    @Provides
    @Singleton
    fun provideFeedDao(cacheDatabase: CacheDatabase): FeedItemDao {
        return cacheDatabase.feedDao()
    }
}