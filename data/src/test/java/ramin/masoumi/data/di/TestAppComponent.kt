package ramin.masoumi.data.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ramin.masoumi.data.ApiTest
import ramin.masoumi.data.DataConfig
import ramin.masoumi.data.DataSourceTest
import javax.inject.Singleton

@Component(modules = [TestDataSourceModule::class])
@Singleton
internal interface TestAppComponent {

    fun inject(apiTest: ApiTest)

    fun inject(dataSourceTest: DataSourceTest)
    @Component.Builder
    interface Builder {
        fun build(): TestAppComponent

        @BindsInstance
        fun context(context: Context): Builder
        @BindsInstance
        fun config(config: DataConfig): Builder
    }
}