package ramin.masoumi.data.di

import dagger.Binds
import dagger.Module
import ramin.masoumi.data.source.DataSource
import ramin.masoumi.data.source.DataSourceImpl
import javax.inject.Singleton

@Module(includes = [NetworkModule::class, TestDatabaseModule::class])
internal abstract class TestDataSourceModule {
    /**
     * provides datasource
     */
    @Binds
    @Singleton
    abstract fun provideDataSource(impl: DataSourceImpl): DataSource

}