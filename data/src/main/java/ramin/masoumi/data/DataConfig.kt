package ramin.masoumi.data

/**
 * Config data class that is used to configure this data module
 * @param baseUrl url for network calls
 */
data class DataConfig(val baseUrl: String)