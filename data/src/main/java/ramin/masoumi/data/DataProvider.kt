package ramin.masoumi.data

import android.content.Context
import ramin.masoumi.data.di.DaggerAppComponent
import ramin.masoumi.data.source.DataSource
import javax.inject.Inject

/**
 * This class is the entry point to the module, it's responsible for
 * creating and exposing data calls
 */
class DataProvider(context: Context, config: DataConfig) {
    @Inject internal lateinit var dataSource: DataSource

    init {
        val component = DaggerAppComponent.builder().context(context).config(config).build()
        component.inject(this)
    }

    fun getFeed(tags: List<String>) = dataSource.getFeed(tags)

    fun getFeedItem(link: String) = dataSource.getFeedItem(link)
}