package ramin.masoumi.data.model

import com.squareup.moshi.JsonClass
import java.time.Instant

/**
 * Model Representing a feed
 */
@JsonClass(generateAdapter = true)
data class Feed(
    val title: String,
    val link: String,
    val description: String,
    val modified: Instant,
    val generator: String,
    val items: List<FeedItem>
)