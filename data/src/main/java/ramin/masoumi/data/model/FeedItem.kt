package ramin.masoumi.data.model

import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import ramin.masoumi.data.net.FeedTag
import ramin.masoumi.data.net.MediaM
import java.time.Instant
import java.time.ZonedDateTime

/**
 * Model Representing a feed item
 */
@JsonClass(generateAdapter = true)
data class FeedItem(
    val title: String,
    @PrimaryKey
    val link: String,
    @MediaM
    val media: String,
    @Json(name = "date_taken")
    val dateTaken: ZonedDateTime,
    val description: String,
    val published: Instant,
    val author: String,
    @Json(name = "author_id")
    val authorId: String,
    @FeedTag
    val tags: List<String>
)