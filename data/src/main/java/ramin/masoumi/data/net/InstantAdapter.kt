package ramin.masoumi.data.net

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

/**
 * Custom JsonAdapter to parse the date provided in a LocalDateTime
 */
internal class InstantAdapter : JsonAdapter<Instant>() {
    companion object {
        private val FORMATTER = DateTimeFormatter.ISO_INSTANT
    }

    override fun toJson(writer: JsonWriter, value: Instant?) {
        if (value == null) {
            writer.nullValue()
        } else {
            val string = FORMATTER.format(value)
            writer.value(string)
        }
    }

    override fun fromJson(reader: JsonReader): Instant? {
        if (reader.peek() == JsonReader.Token.NULL) {
            return reader.nextNull<Instant>()
        }
        val string = reader.nextString()
        val temp = try {
            FORMATTER.parse(string)
        }catch (e : DateTimeParseException){
            reader.nextNull<LocalDateTime>()
        }
        return Instant.from(temp)
    }
}