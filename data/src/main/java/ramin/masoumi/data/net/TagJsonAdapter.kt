package ramin.masoumi.data.net

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonQualifier
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson

@JsonQualifier
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
internal annotation class FeedTag

/**
 * Custom JsonAdapter to map tags to list
 */
@Suppress("unused")
internal class TagJsonAdapter: JsonAdapter<List<String>>() {
    @ToJson
    override fun toJson(writer: JsonWriter, @FeedTag str: List<String>?) {
        writer.name("tags").value(str?.joinToString(" "))
    }

    @FromJson
    @FeedTag
    override fun fromJson(reader: JsonReader): List<String> {
        val stringList = reader.nextString()
        return stringList.split(" ")
    }

}