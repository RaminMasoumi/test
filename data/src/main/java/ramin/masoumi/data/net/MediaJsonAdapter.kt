package ramin.masoumi.data.net

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonQualifier
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson
import java.io.IOException

@JsonQualifier
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
internal annotation class MediaM

/**
 * Custom JsonAdapter to map media{m} to string
 */
@Suppress("unused")
internal class MediaJsonAdapter: JsonAdapter<String>() {
    @ToJson
    override fun toJson(writer: JsonWriter, @MediaM str: String?) {
        writer.beginObject()
        writer.name("m").value(str)
        writer.endObject()
    }

    @FromJson
    @MediaM
    override fun fromJson(reader: JsonReader): String {
        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.nextName()) {
                "m" -> {

                    val link = reader.nextString()
                    reader.endObject()
                    return link
                }
                else -> reader.skipValue()
            }
        }
        reader.endObject()
        throw IOException("malformed json, m not found")
    }

}