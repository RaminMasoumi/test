package ramin.masoumi.data.net

import okhttp3.internal.toHeaderList
import ramin.masoumi.data.CallResult
import ramin.masoumi.data.model.Feed
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit Api interface
 */
internal interface Api{
    /**
     * get list of feeds that match one of more of the tags
     * @param tags comma separated list of tags
     * @return response of list of feeds
     */
    @GET("services/feeds/photos_public.gne?format=json&nojsoncallback=1&safe_search=1")
    suspend fun getFeed(@Query("tags", encoded = true) tags : String) : Response<Feed>

    companion object {
        /**
         * Helper method which can be used to safely do an API call from the interface
         * @param call the actual suspending network call
         * @return CallResult instance with transformed header and data
         */
        suspend fun <T : Any> apiCall(call: suspend () -> Response<T>): CallResult<T> {
            try{
                val response = call.invoke()
                val body = response.body()
                if (response.isSuccessful) {
                    val headers = response.headers().toHeaderList().associate {
                        it.name.utf8() to it.value.utf8()
                    }
                    return CallResult.success(body, headers,"",response.code())
                }
                return CallResult.error(response.message(), response.code(),null)
            }catch (e : Exception){
                return CallResult.error(e.message ?: e.toString())
            }
        }
    }
}