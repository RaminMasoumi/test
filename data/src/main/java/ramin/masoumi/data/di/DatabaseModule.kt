package ramin.masoumi.data.di

import android.content.Context
import androidx.room.Room.databaseBuilder
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import ramin.masoumi.data.persist.CacheDatabase
import ramin.masoumi.data.persist.FeedItemDao
import ramin.masoumi.data.persist.Converter
import javax.inject.Singleton

/**
 * Dagger providers for Database components
 */
@Module
internal class DatabaseModule {
    /**
     * provides room database instance
     */
    @Singleton
    @Provides
    fun providesRoomDatabase(context: Context, moshi: Moshi): CacheDatabase {
        return databaseBuilder(
            context,
            CacheDatabase::class.java, "cache"
        ).addTypeConverter(Converter(moshi))
            .fallbackToDestructiveMigration().build()
    }

    /**
     * provides room FeedDao
     */
    @Provides
    @Singleton
    fun provideFeedDao(cacheDatabase: CacheDatabase): FeedItemDao {
        return cacheDatabase.feedDao()
    }
}