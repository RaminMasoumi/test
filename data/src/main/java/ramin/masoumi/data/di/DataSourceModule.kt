package ramin.masoumi.data.di

import dagger.Binds
import dagger.Module
import ramin.masoumi.data.source.DataSource
import ramin.masoumi.data.source.DataSourceImpl
import javax.inject.Singleton

/**
 * Dagger providers for DataSource components
 */
@Module(includes = [NetworkModule::class, DatabaseModule::class])
internal abstract class DataSourceModule {
    /**
     * provides datasource
     */
    @Binds
    @Singleton
    abstract fun provideDataSource(impl: DataSourceImpl): DataSource

}