package ramin.masoumi.data.di

import android.content.Context
import android.content.pm.ApplicationInfo
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ramin.masoumi.data.DataConfig
import ramin.masoumi.data.net.Api
import ramin.masoumi.data.net.InstantAdapter
import ramin.masoumi.data.net.MediaJsonAdapter
import ramin.masoumi.data.net.TagJsonAdapter
import ramin.masoumi.data.net.ZonedDateTimeAdapter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.time.Instant
import java.time.ZonedDateTime
import javax.inject.Named
import javax.inject.Singleton


/**
 * Dagger providers for network components
 */
@Module
internal class NetworkModule {
    /**
     * provides LoggingInterceptor to set logging config in okHttp interface
     */
    @Provides
    @Singleton
    fun provideLoggingInterceptor(@Named("FlagNetworkExtensiveLog") extensiveLog : Boolean)
    : HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (extensiveLog)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
        return interceptor
    }

    /**
     * provides okHttp instance
     */
    @Provides
    @Singleton
    fun provideOkHttp(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
    }

    /**
     * provides Moshi instance configured with Json converters
     */
    @Provides
    @Singleton
    fun provideMoshi() : Moshi = Moshi.Builder().add(MediaJsonAdapter())
        .add(Instant::class.java, InstantAdapter().nullSafe())
        .add(ZonedDateTime::class.java, ZonedDateTimeAdapter().nullSafe())
        .add(TagJsonAdapter()).build()

    /**
     * provides retrofit instance
     */
    @Provides
    @Singleton
    fun provideRetrofit(httpClient: OkHttpClient, @Named("Url") url : HttpUrl, moshi : Moshi)
    : Retrofit {
        val retroBuilder = Retrofit.Builder()
            .client(httpClient)
            .baseUrl(url)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
        return retroBuilder.build()
    }

    /**
     * provides retrofit Api for network calls
     */
    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    /**
     * provides base Url for retrofit
     */
    @Provides
    @Singleton
    @Named("Url")
    fun provideUrl(config: DataConfig): HttpUrl {
        return config.baseUrl.toHttpUrl()
    }

    /**
     * provides logging flag to turn extensive logging on/off
     */
    @Provides
    @Named("FlagNetworkExtensiveLog")
    fun provideLogFlag(context: Context) : Boolean {
        return context.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0
    }
}