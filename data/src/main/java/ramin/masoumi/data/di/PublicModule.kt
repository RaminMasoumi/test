package ramin.masoumi.data.di

import dagger.Module

@Module(includes = [DataSourceModule::class])
interface PublicModule