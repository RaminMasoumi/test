package ramin.masoumi.data.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ramin.masoumi.data.DataConfig
import ramin.masoumi.data.DataProvider
import javax.inject.Singleton

@Component(modules = [PublicModule::class])
@Singleton
internal interface AppComponent {

    fun inject(provider : DataProvider)
    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun context(context: Context): Builder
        @BindsInstance
        fun config(config: DataConfig): Builder
    }
}