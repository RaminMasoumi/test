package ramin.masoumi.data.source

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import ramin.masoumi.data.CallResult
import ramin.masoumi.data.model.FeedItem
import ramin.masoumi.data.net.Api
import ramin.masoumi.data.persist.FeedItemDao
import javax.inject.Inject

/**
 * DataSource to fetch data from a local or network source
 */
internal interface DataSource {
    /**
     * Get feedItems that have one of more of the tags
     * @param tags list of tags
     * @return Flow that will update when data is available
     */
    fun getFeed(tags: List<String>) : Flow<CallResult<List<FeedItem>>>

    /**
     * get the saved feedItem,
     * @param link of the feedItem
     * @return flow of one feedItem
     */
    fun getFeedItem(link: String) : Flow<CallResult<FeedItem>>
}

internal class DataSourceImpl@Inject constructor(
    private val api: Api,
    private val dao: FeedItemDao
) : DataSource{

    override fun getFeed(tags: List<String>): Flow<CallResult<List<FeedItem>>> {
        return getNetworkFlow({
            val res = Api.apiCall { api.getFeed(tags.joinToString(" ")) }
            res.copyConvert { it?.items }
        })
        { items, _ ->
            dao.insertFeedItemsWithTags(items)
        }
    }

    override fun getFeedItem(link: String): Flow<CallResult<FeedItem>> =
        dao.loadFeedItem(link).map {
            val feedItem = it?.toFeedItem()
            if (it != null) {
                CallResult.success(feedItem)
            } else {
                CallResult.error()
            }
        }.onStart { CallResult.loading<FeedItem>() }

    /**
     * Helper method that executes and emits network calls as a single Flow
     * @param networkCall method containing network call invocation
     * @param saveCallResult optional method responsible for persisting network results to database
     * @return Flow that will report data state
     */
    private fun <T> getNetworkFlow(
        networkCall: suspend () -> CallResult<T>,
        saveCallResult: (suspend (T, extra: Map<String, String>) -> Unit)? = null
    ): Flow<CallResult<T>> =
        flow<CallResult<T>> {
            coroutineScope {
                val responseCall = async(Dispatchers.IO) { networkCall() }
                val response = responseCall.await()
                if (response.isSuccess() && response.data != null) {
                    emit(CallResult.success(response.data))
                    saveCallResult?.invoke(response.data, response.extra)
                } else if (response.isFail()) {
                    emit(CallResult.error(response.message, response.code))
                }
            }
        }.onStart { emit(CallResult.loading()) }
}