package ramin.masoumi.data.persist.table

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.Instant
import java.time.ZonedDateTime

/**
 * Model Representing a feed item table
 */
@Entity(tableName = "FeedItem")
data class DbFeedItem(
    @PrimaryKey
    val link: String,
    val title: String,
    val media: String,
    val dateTaken: ZonedDateTime,
    val description: String,
    val published: Instant,
    val author: String,
    val authorId: String
)