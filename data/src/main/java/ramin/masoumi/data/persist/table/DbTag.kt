package ramin.masoumi.data.persist.table

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Model representing FeedItem's Tag table
 */
@Entity(tableName = "Tag")
data class DbTag(
    @PrimaryKey
    val tag: String
)