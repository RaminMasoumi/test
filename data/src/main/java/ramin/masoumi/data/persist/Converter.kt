package ramin.masoumi.data.persist

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import java.time.Instant
import java.time.ZonedDateTime

/**
 * Converters for Room Database
 */
@ProvidedTypeConverter
internal class Converter(private val moshi: Moshi) {
    @TypeConverter
    fun fromStringToInstant(value : String?): Instant? {
        if(value == null)
            return null
        val adapter = moshi.adapter(Instant::class.java)
        return adapter.fromJson(value)
    }

    @TypeConverter
    fun fromInstantToString(dateTime: Instant?): String?{
        if(dateTime == null)
            return null
        val adapter = moshi.adapter(Instant::class.java)
        return adapter.toJson(dateTime)
    }

    @TypeConverter
    fun fromStringToZonedDateTime(value : String?): ZonedDateTime? {
        if(value == null)
            return null
        val adapter = moshi.adapter(ZonedDateTime::class.java)
        return adapter.fromJson(value)
    }

    @TypeConverter
    fun fromZonedDateTimeToString(dateTime: ZonedDateTime?): String? {
        if (dateTime == null)
            return null
        val adapter = moshi.adapter(ZonedDateTime::class.java)
        return adapter.toJson(dateTime)
    }
}
