package ramin.masoumi.data.persist.table

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.Companion.CASCADE
import androidx.room.Index

/**
 * Model Representing a feed and tag cross reference item table
 */

@Entity(
    primaryKeys = ["tag", "link"], tableName = "FeedItemTagRef",
    foreignKeys = [
        ForeignKey(
            entity = DbTag::class,
            parentColumns = ["tag"],
            childColumns = ["tag"],
            onDelete = CASCADE
        ),
        ForeignKey(
            entity = DbFeedItem::class,
            parentColumns = ["link"],
            childColumns = ["link"],
            onDelete = CASCADE
        )],
    indices = [Index(value = ["tag"]),
        Index(value = ["link"])]
)
data class DbFeedItemTagCrossRef(
    val tag: String,
    val link: String
)