package ramin.masoumi.data.persist.table

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import ramin.masoumi.data.model.FeedItem

/**
 * Model representing a full FeedItem with its tags
 */
data class DbFeedItemWithTag(
    @Embedded val feedItem: DbFeedItem,
    @Relation(
        parentColumn = "link",
        entityColumn = "tag",
        associateBy = Junction(DbFeedItemTagCrossRef::class)
    )
    val tags: List<DbTag>
){
    fun toFeedItem() =
        FeedItem(title = feedItem.title, link = feedItem.link, media = feedItem.media,
            dateTaken = feedItem.dateTaken, description = feedItem.description,
            published = feedItem.published, author = feedItem.author,
            authorId = feedItem.authorId, tags = tags.map { it.tag })
}