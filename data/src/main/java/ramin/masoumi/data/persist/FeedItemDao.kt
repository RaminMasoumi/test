package ramin.masoumi.data.persist

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow
import ramin.masoumi.data.model.FeedItem
import ramin.masoumi.data.persist.table.DbFeedItem
import ramin.masoumi.data.persist.table.DbFeedItemTagCrossRef
import ramin.masoumi.data.persist.table.DbFeedItemWithTag
import ramin.masoumi.data.persist.table.DbTag

/**
 * Room Dao interface , provides methods for persisting FeedItems
 */
@Dao
internal interface FeedItemDao {

    /**
     * get FeedItem from the database using its id
     * @param link of the FeedItem
     * @return FeedItem
     */
    @Transaction
    @Query("SELECT * FROM FeedItem WHERE  link = :link LIMIT 1")
    fun loadFeedItem(link : String) : Flow<DbFeedItemWithTag?>

    /**
     * get all FeedItems
     * @return list of FeedItems
     */
    @Transaction
    @Query("SELECT * FROM FeedItem " +
            "JOIN FeedItemTagRef on FeedItem.link == FeedItemTagRef.link " +
            "WHERE :tags IN tag")
    fun loadFeedItems(tags: List<String>) : Flow<List<DbFeedItemWithTag>>

    /**
     * insert a single FeedItem row
     * @param feedItem to insert
     * @return row number
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFeedItem(feedItem : DbFeedItem) : Long

    /**
     * insert a single FeedItem and updates it's tag
     * @param feedItem to insert
     */
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFeedItemWithTag(feedItem : FeedItem) {
        val dBFeed = DbFeedItem(
            title = feedItem.title,
            link = feedItem.link,
            media = feedItem.media,
            dateTaken = feedItem.dateTaken,
            description = feedItem.description,
            published = feedItem.published,
            author = feedItem.author,
            authorId = feedItem.authorId
        )
        val tags = feedItem.tags.filter { it.isNotBlank() }.map { DbTag(it) }
        val crossRef = tags.map { DbFeedItemTagCrossRef(tag = it.tag, link = dBFeed.link) }
        insertFeedItem(dBFeed)
        insertTags(tags)
        insertFeedTagCrossRefs(crossRef)
    }

    /**
     * insert feedItems
     * @param feedItems to insert
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFeedItems(feedItems : List<DbFeedItem>)

    /**
     * insert multiple feedItems
     * @param feedItems to insert
     */
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFeedItemsWithTags(feedItems: List<FeedItem>) {
        val tagsToAdd = mutableListOf<DbTag>()
        val refsToAdd = mutableListOf<DbFeedItemTagCrossRef>()
        val dbFeeds = feedItems.map { feedItem ->
            val dBFeed = DbFeedItem(
                title = feedItem.title,
                link = feedItem.link,
                media = feedItem.media,
                dateTaken = feedItem.dateTaken,
                description = feedItem.description,
                published = feedItem.published,
                author = feedItem.author,
                authorId = feedItem.authorId
            )
            val tags = feedItem.tags.filter { it.isNotBlank() }.map { DbTag(it) }
            val crossRef = tags.map { DbFeedItemTagCrossRef(tag = it.tag, link = dBFeed.link) }
            tagsToAdd.addAll(tags)
            refsToAdd.addAll(crossRef)
            dBFeed
        }
        insertFeedItems(dbFeeds)
        insertTags(tagsToAdd)
        insertFeedTagCrossRefs(refsToAdd)
    }

    /**
     * insert multiple tags
     * @param tags to insert
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertTags(tags : List<DbTag>)

    /**
     * insert multiple feedTagCrossRefs
     * @param feedItemTagCrossRefs to insert
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFeedTagCrossRefs(feedItemTagCrossRefs: List<DbFeedItemTagCrossRef>)

    /**
     * delete a FeedItem from database
     * @param link of the FeedItem to delete
     */
    @Query("DELETE FROM FeedItem WHERE link = :link")
    suspend fun deleteFeedItem(link: String)
}