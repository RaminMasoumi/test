package ramin.masoumi.data.persist

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ramin.masoumi.data.persist.table.DbFeedItem
import ramin.masoumi.data.persist.table.DbFeedItemTagCrossRef
import ramin.masoumi.data.persist.table.DbTag

/**
 * Room database
 */
@Database(entities = [DbFeedItem::class, DbTag::class, DbFeedItemTagCrossRef::class], version = 1)
@TypeConverters(Converter::class)
internal abstract class CacheDatabase : RoomDatabase(){
    abstract fun feedDao() : FeedItemDao
}